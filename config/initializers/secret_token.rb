# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
DemoApp::Application.config.secret_key_base = 'c7a0a2bba5bc83bdd8d6bf31199cf0791bf81bcacd4043057c83d82a2bb630578beb57ee68f1c61c354a513dfc3d9c8dc4f58f725e8c746a5bcf8e996033c800'
